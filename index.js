const Dictionary = require('./Dictionary')

var input = process.argv
var option
// For word of the day
if (input.length == 2) {
  option = 6
  var dictionary = new Dictionary(option, '')
}else {
  switch (input[2]) {
    case 'def':
      option = 1
      var dictionary = new Dictionary(option, input[3])
      break

    case 'syn':
      option = 2
      var dictionary = new Dictionary(option, input[3])
      break

    case 'ant':
      option = 3
      var dictionary = new Dictionary(option, input[3])
      break

    case 'ex':
      option = 4
      var dictionary = new Dictionary(option, input[3])
      break

    case 'play':
      option = 7
      var dictionary = new Dictionary(option, '')
      break

    case 'dict': // Full Details
      option = 5
      var dictionary = new Dictionary(option, input[3])
      break
    default:
    option = 5
      var dictionary = new Dictionary(option, input[2])
      break
  }
}

dictionary.on('completed', () => {
  switch (option) {
    case 1:
      printDefinition()
      break
    case 2:
      printSynonyms()
      break
    case 3:
      printAntonyms()
      break
    case 4:
      printExamples()
      break
    case 5:
      printAll()
      break
    case 6:
      printAll()
      break
  }
})
/*
    All the printing functions
*/
function printDefinition () {
  console.log('====== Definition ======')
  dictionary.definitions.forEach(element => {
    console.log(element, '\n')
  })
}
function printSynonyms () {
  console.log('====== Synonyms ======')
  if (dictionary.synonyms.length > 0) {
    dictionary.synonyms.forEach(element => {
      console.log(element, '\n')
    })
  }else {
    console.log('No Synonyms\n')
  }
}
function printAntonyms () {
  console.log('====== Antonyms ======')
  if (dictionary.antonyms.length > 0) {
    dictionary.antonyms.forEach(element => {
      console.log(element, '\n')
    })
  }else {
    console.log('No Antonyms\n')
  }
}
function printExamples () {
  console.log('====== Examples ======')
  if (dictionary.examples.length > 0) {
    dictionary.examples.forEach(element => {
      console.log(element, '\n')
    })
  }else {
    console.log('No Examples\n')
  }
}
function printWord () {
  console.log('====== Word ======')
  console.log(dictionary.word, '\n')
}
function printAll () {
  printWord()
  printDefinition()
  printSynonyms()
  printAntonyms()
  printExamples()
}
