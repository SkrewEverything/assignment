const readline = require('readline')

class Game {
  constructor (obj) {
    this.object = obj
    // Random Array to provide hints
    this.randomArray = []
    var temp = []
    if (obj.syn.length != 0) {
      temp = temp.concat(obj.syn)
    }
    if (obj.ant.length != 0) {
      temp = temp.concat(obj.ant)
    }
    temp = temp.concat(obj.def)
    this.randomArray = temp.slice()
    this.randomArray = this.shuffle(this.randomArray)
    // Index for randomArray
    this.count = 0
    // Used to check whether the user entered answer is correct or not
    this.answers = new Map()
    obj.syn.forEach(element => {
      this.answers.set(element, true)
    })
    this.answers.set(obj.word, true)
    this.start()
  }

  start () {
    var rl = readline.createInterface(
      {
        input: process.stdin,
        output: process.stdout
      }
    )

    console.log('In Game')
    rl.setPrompt('Game> ')
    console.log('Hint: ', this.randomArray[this.count])
    this.count++
    rl.prompt()
    // To use `this` inside callback
    var self = this
    rl.on('line', line => {
      var input = String(line)
      if (self.answers.get(input)) {
        console.log('Correct Answer!')
        rl.close()
      }

      switch (input) {
        case '1':
          rl.prompt()
          break

        case '2':
          if (self.count == self.randomArray.length) {
            self.count = 0
          }
          console.log(self.randomArray[self.count])
          self.count++
          rl.prompt()
          break

        case '3':
          console.log('====== Word ======')
          console.log(self.object.word, '\n')
          console.log('====== Definition ======')
          self.object.def.forEach(element => {
            console.log(element, '\n')
          })
          console.log('====== Synonyms ======')
          if (self.object.syn.length > 0) {
            self.object.syn.forEach(element => {
              console.log(element, '\n')
            })
          }else {
            console.log('No Synonyms\n')
          }
          console.log('====== Antonyms ======')
          if (self.object.ant.length > 0) {
            self.object.ant.forEach(element => {
              console.log(element, '\n')
            })
          }else {
            console.log('No Antonyms\n')
          }
          console.log('====== Examples ======')
          if (self.object.ex.length > 0) {
            self.object.ex.forEach(element => {
              console.log(element, '\n')
            })
          }else {
            console.log('No Examples\n')
          }
          rl.close()
          break

        default:

          console.log('1). Try Again\n2). Hint\n3). Quit')
          rl.prompt()
      }
    }).on('close', function () {
      process.exit(0)
    })
  }

  // Shuffles the Array for providing hints
  shuffle (array) {
    var tmp, current, top = array.length

    if (top) while(--top) {
        current = Math.floor(Math.random() * (top + 1))
        tmp = array[current]
        array[current] = array[top]
        array[top] = tmp
    }

    return array
  }
}

module.exports = Game
