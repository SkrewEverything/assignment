const request = require('request');
const Game = require('./Game')
const EventEmitter = require('events');

class Dictionary extends EventEmitter {
    constructor(option, word, rl) {
        super();
        this.word = word;
        this.option = option;
        this.definitions = [];
        this.synonyms = [];
        this.antonyms = [];
        this.examples = [];
        this.findCategory();


    }

    async findCategory() {
        switch (this.option) {
            case 1:
                await this.getDefinitions();
                break;

            case 2:
                await this.getSynonyms();
                break;

            case 3:
                await this.getAntonyms();
                break;

            case 4:
                await this.getExamples();
                break;

            case 5:
                await this.getFullDetails();
                break;

            case 6:
                await this.getWordOfTheDay();
                await this.getFullDetails();
                break;

            case 7:
                await this.startWordGame();
                console.log(this.word);
                await this.getFullDetails();
                var obj = {
                    word: this.word,
                    syn: this.synonyms.slice(),
                    ant: this.antonyms.slice(),
                    def: this.definitions.slice(),
                    ex: this.examples.slice(),
                }
                var game = new Game(obj);
                break;
        }
        // Emits when all the properties are ready for display
        this.emit('completed');
    }

    async getDefinitions() {
        var options = {
            url: `http://api.wordnik.com:80/v4/word.json/${this.word}/definitions?limit=5&includeRelated=true&useCanonical=false&includeTags=false&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5`,
            method: 'GET',
        }
        // To use `this` inside callback
        var self = this;
        await this.sendRequest(options).then(function (body) {
            var definitions = []
            body.forEach(element => {
                definitions.push(element.text);
            });
            self.definitions = definitions.slice();
            //console.log("=========== Definitions ===========");
            //console.log(definitions);
        }).catch(error => console.log(error));
        return new Promise(function (resolve, reject) {
            resolve();
        });
    }

    async getSynonyms() {
        var options = {
            url: `http://api.wordnik.com:80/v4/word.json/${this.word}/relatedWords?useCanonical=false&relationshipTypes=synonym&limitPerRelationshipType=1000&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5`,
            method: 'GET',
        }
        // To use `this` inside callback
        var self = this;
        await this.sendRequest(options).then(function (body) {
            var synonyms = [];
            if (body.length > 0) {
                body[0].words.forEach(element => {
                    synonyms.push(element);
                });
                self.synonyms = synonyms.slice();
                //console.log("=========== Synonyms ===========");
                //console.log(synonyms);
            }
        }).catch(error => console.log(error));
        return new Promise(function (resolve, reject) {
            resolve();
        });
    }

    async getAntonyms() {
        var options = {
            url: `http://api.wordnik.com:80/v4/word.json/${this.word}/relatedWords?useCanonical=false&relationshipTypes=antonym&limitPerRelationshipType=1000&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5`,
            method: 'GET',
        }
        // To use `this` inside callback
        var self = this;
        await this.sendRequest(options).then(function (body) {
            var antonyms = []
            if (body.length > 0) {
                body[0].words.forEach(element => {
                    antonyms.push(element);
                });
                self.antonyms = antonyms.slice();
                //console.log("=========== Antonyms ===========");
                //console.log(antonyms);
            }
        }).catch(error => console.log(error));
        return new Promise(function (resolve, reject) {
            resolve();
        });
    }

    async getExamples() {
        var options = {
            url: `http://api.wordnik.com:80/v4/word.json/${this.word}/examples?includeDuplicates=false&useCanonical=false&skip=0&limit=5&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5`,
            method: 'GET',
        }
        // To use `this` inside callback
        var self = this;
        await this.sendRequest(options).then(function (body) {
            var examples = []
            body.examples.forEach(element => {
                examples.push(element.text);
            });
            self.examples = examples.slice();
            //console.log("=========== Examples ===========");
            //console.log(examples);
        }).catch(error => console.log(error));
        return new Promise(function (resolve, reject) {
            resolve();
        });
    }

    async getFullDetails() {
        await this.getDefinitions();
        await this.getSynonyms();
        await this.getAntonyms();
        await this.getExamples();
        return new Promise(function (resolve, reject) {
            resolve();
        });
    }

    async getWordOfTheDay() {
        var options = {
            url: 'http://api.wordnik.com:80/v4/words.json/wordOfTheDay?api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5',
            method: 'GET',
        }
        // To use `this` inside callback
        var self = this;
        await this.sendRequest(options).then(function (body) {
            self.word = body.word;
            return new Promise(resolve => resolve(body.word));
        }).catch(error => console.log(error));
    }

    async startWordGame() {
        var options = {
            url: 'http://api.wordnik.com:80/v4/words.json/randomWord?hasDictionaryDef=true&minCorpusCount=0&maxCorpusCount=-1&minDictionaryCount=1&maxDictionaryCount=-1&minLength=5&maxLength=-1&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5',
            method: 'GET',
        }
        // To use `this` inside callback
        var self = this;
        await this.sendRequest(options).then(function (body) {
            self.word = body.word;
            return new Promise(resolve => resolve(body.word));
        }).catch(error => console.log(error));
    }

    sendRequest(options) {
        return new Promise(function (resolve, reject) {
            request(options, (error, response, body) => {
                if (!error && response.statusCode == 200) {
                    resolve(JSON.parse(body));
                } else {
                    reject(error);
                }
            });
        });
    }


}

module.exports = Dictionary;